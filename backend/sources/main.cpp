#define BOOST_ALL_NO_LIB
#include "crow_all.h"

#include <iostream>

#include <filesystem>
#include <fstream>
#include <string>
#include <stdint.h>
#include <chrono>
#include <unordered_set>

using namespace std::chrono;

std::string getFileContent(const char* path) {
    constexpr auto read_size = std::size_t{ 4096 };
    auto stream = std::ifstream{ path };
    stream.exceptions(std::ios_base::badbit);

    auto out = std::string{};
    auto buf = std::string(read_size, '\0');
    while (stream.read(&buf[0], read_size)) {
        out.append(buf, 0, stream.gcount());
    }
    out.append(buf, 0, stream.gcount());
    return out;
}

struct Config {
    int elements_per_update = 10;
    int update_frequency_milliseconds = 100;
    std::vector<std::string> symbols;
};

Config* readConfigs() 
{
    auto config = new Config();
    auto data = getFileContent("./config/config.txt");
    
    if (data.empty()) {
        return config;
    }

    auto x = crow::json::load(data);
    if (!x) {
        return config;
    }

    if (x.has("elements_per_update")) {
        config->elements_per_update = (int) x["elements_per_update"];
    }
    if (x.has("update_frequency_milliseconds")) {
        config->update_frequency_milliseconds = (int) x["update_frequency_milliseconds"];
    }

    if (x.has("symbols")) {
        auto obj = x["symbols"];
        if (obj.t() == crow::json::type::List) {
            for (auto it = obj.begin(); it != obj.end(); it++) {
                std::string value = it->s();
                config->symbols.push_back(value);
            }
        }
    }
    return config;
}
 
class MyData {
public: 
    std::mutex userMtx;
    std::unordered_set<crow::websocket::connection*> users;

    typedef std::pair<uint64_t, double> ElementInfo;
    std::atomic_int stopping;
    std::unique_ptr<Config> config;
    std::map<std::string, std::vector<ElementInfo>> elementMap;
    int last_index = 0;
    std::mutex lock;

    void updateElements()
    {
        std::chrono::milliseconds sleepTime;
        while (!stopping) {   

            std::vector<crow::json::wvalue> updateList;
            {
                std::lock_guard<std::mutex> lockGuard(lock);
                if (config->symbols.size() <= config->elements_per_update) {
                    for (auto it = config->symbols.begin(); it != config->symbols.end(); ++it) {
                        auto info = updateItem(*it);
                        updateList.push_back(createJsonValue(*it, info));
                    }
                }
                else {
                    for (auto i = 0; i < config->elements_per_update; i++) {
                        int index = (i + last_index) % config->symbols.size();
                        auto info = updateItem(config->symbols.at(index));
                        updateList.push_back(createJsonValue(config->symbols.at(index), info));

                    }
                    last_index = (config->elements_per_update + last_index) % config->symbols.size();
                }
                sleepTime = std::chrono::milliseconds(config->update_frequency_milliseconds);
            }

            crow::json::wvalue result;
            result = std::move(updateList);
            auto message = crow::json::dump(result);

            {   
                std::lock_guard<std::mutex> usrLockGuard(userMtx);
                for (auto it = users.begin(); it != users.end(); ++it) {
                    (*it)->send_text(message);
                }
            }

            std::this_thread::sleep_for(sleepTime);
        }
    }

    ElementInfo updateItem(std::string symbol) {
        auto result = elementMap.find(symbol);
        if (result == elementMap.end()) {
            elementMap[symbol] = std::vector<ElementInfo>();
            result = elementMap.find(symbol);
        }
        
        std::pair<uint64_t, double> data;
        data.first = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
        data.second = rand() % 100000;
        result->second.push_back(data);

        while (result->second.size() > 0) {
            const auto& firstItem = result->second.at(0);
            if ((data.first - firstItem.first) < 5 * 60 * 1000) {
                break;
            }
            result->second.erase(result->second.begin());
        }
        return data;
    }

    crow::json::wvalue createJsonValue(const std::string& symbol, const ElementInfo& info) {
        crow::json::wvalue x;
        x["symbol"] = symbol;
        x["time"] = std::to_string(info.first);
        x["price"] = info.second;
        return x;
    }

    static void runTask(MyData* data) {
        data->updateElements();
    }
};

void addHeader(crow::response& res) {
    res.add_header("Access-Control-Allow-Origin", "*");
    res.add_header("Access-Control-Allow-Methods", "POST, OPTIONS");
    res.add_header("Access-Control-Allow-Headers", "Content-Type");
    res.add_header("Access-Control-Max-Age", "86400");
}

int main()
{
    crow::SimpleApp app;
    MyData data;
    data.config.reset(readConfigs());

    std::thread t1(MyData::runTask, &data);

    CROW_ROUTE(app, "/ws")
        .websocket()
        .onopen([&](crow::websocket::connection& conn) {
        CROW_LOG_INFO << "new websocket connection";
        std::lock_guard<std::mutex> _(data.userMtx);
        data.users.insert(&conn);
            })
        .onclose([&](crow::websocket::connection& conn, const std::string& reason) {
                CROW_LOG_INFO << "websocket connection closed: " << reason;
                std::lock_guard<std::mutex> _(data.userMtx);
                data.users.erase(&conn);
        });


    CROW_ROUTE(app, "/update-config")
        .methods("OPTIONS"_method, "POST"_method)
        ([&](const crow::request& req) {
            if (req.method == crow::HTTPMethod::Options) {
                crow::response res(200);
                addHeader(res);
                return res;
            }

            auto x = crow::json::load(req.body);

            if (!x || !x.has("update_frequency_milliseconds") || ((int)x["update_frequency_milliseconds"]) < 100) {
                crow::response res (400);
                addHeader(res);
                return res;
            }

            {                   
                std::lock_guard<std::mutex> lockGuard(data.lock);
                data.config->update_frequency_milliseconds = (int)x["update_frequency_milliseconds"];
            }
            
            crow::json::wvalue y;
            y["result"] = true;
            crow::response res(y);
            addHeader(res);
            return res;
        });


    app.port(18080).multithreaded().run();
    data.stopping = 1;
    t1.join();
}