import "./App.css";

import axios from "axios";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TableComponent from "./TableComponent";
import TextField from "@material-ui/core/TextField";
import { useRef } from "react";
import Container from "@material-ui/core/Container";
function App() {
  const frequence = useRef(null);
  const symbol = useRef("");
  const targetValue = useRef(1000);
  return (
    <div className="App">
      <Container maxWidth="md">
        <header className="App-header">LHFT</header>
        <div className="app-content">
          <Grid
            container
            direction="row"
            justify="center"
            alignItems="flex-start"
          >
            <Grid item>
              <TextField
                inputRef={frequence}
                id="freq"
                label="Freq (ms)"
                type="number"
                InputLabelProps={{
                  shrink: true,
                }}
                variant="outlined"
                defaultValue="1000"
                onKeyUp={updateFreq}
                InputProps={{ inputProps: { min: 100, max: 100000 } }}
                onKeyDown={validateInput}
                size="small"
                onChange={onChangeFreq}
              />
              <Button
                variant="contained"
                color="primary"
                onClick={updateBackendFreq}
              >
                Update
              </Button>
              <div className="separator">|</div>
              <TextField
                inputRef={symbol}
                required
                id="symbol"
                label="Symbol"
                defaultValue="AAAA"
                variant="outlined"
                size="small"
              />
              <div className="separator">|</div>
              <TextField
                inputRef={targetValue}
                required
                type="number"
                id="targetValue"
                label="Target"
                defaultValue="20"
                variant="outlined"
                onKeyDown={validateInput}
                InputProps={{ inputProps: { min: 0, max: 100000 } }}
                size="small"
                onChange={onChangeTarget}
              />
              <TableComponent
                targetValue={targetValue}
                symbol={symbol}
              ></TableComponent>
            </Grid>
          </Grid>
        </div>
      </Container>
    </div>
  );

  function updateFreq(e) {
    if (e.key === "Enter") {
      updateBackendFreq();
    }
  }

  function validateInput(e) {
    if (
      e.keyCode === 69 ||
      e.keyCode === 190 ||
      e.keyCode === 109 ||
      e.keyCode === 189
    ) {
      e.preventDefault();
    }
  }

  function updateBackendFreq() {
    axios.defaults.baseURL = "http://localhost:18080";

    axios
      .post(`/update-config`, {
        update_frequency_milliseconds: frequence.current.value,
      })
      .then((res) => {
        if (res?.data?.result) {
          console.log("Update Freq - OK");
        }
      });
  }

  function onChangeTarget(e) {
    if (e.target.value) {
      if (e.target.value < 1) {
        e.target.value = 1;
        e.preventDefault();
      } else if (e.target.value > 100000) {
        e.target.value = 100000;
        e.preventDefault();
      }
    }
  }

  function onChangeFreq(e) {
    if (e.target.value) {
      if (e.target.value < 100) {
        e.target.value = 100;
        e.preventDefault();
      } else if (e.target.value > 100000) {
        e.target.value = 100000;
        e.preventDefault();
      }
    }
  }
}

export default App;
