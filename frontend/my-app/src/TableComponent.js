import React from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableFooter from "@material-ui/core/TableFooter";
import TableRow from "@material-ui/core/TableRow";
import TablePagination from "@material-ui/core/TablePagination";

import { useTheme } from "@material-ui/core/styles";

import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import moment from "moment";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles((theme) => ({
  table: {
    minWidth: 700,
  },
  pagnation: {
    flexShrink: 0,
    marginLeft: 15,
  },
}));

class TableComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      elements: {},
      showData: [],
      page: 0,
      rowsPerPage: 100,
    };
    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
  }

  handleChangePage(event, newPage) {
    let newState = { ...this.state };
    newState.page = newPage;
    this.setState(newState);
  }

  handleChangeRowsPerPage(event) {
    let newState = { ...this.state };
    newState.rowsPerPage = parseInt(event.target.value, 10);
    newState.page = 0;
    this.setState(newState);
  }

  addElement(item) {
    let newState = { ...this.state };
    let currentData = newState.elements[item.symbol];
    var dateTime = moment.unix(item.time / 1000).format("YYYY-MM-DD hh:mm:ss");
    if (currentData == null) {
      currentData = [{ price: item.price, time: dateTime }];
    } else {
      currentData = [{ price: item.price, time: dateTime }, ...currentData];
    }
    if (currentData.length > 500) {
      currentData.pop();
    }
    newState.elements[item.symbol] = currentData;
    return newState;
  }

  constructShowList(newState) {
    if (!newState.elements.hasOwnProperty(this.props.symbol.current.value)) {
      return [];
    }

    return newState.elements[this.props.symbol.current.value].map((x, id) => {
      return {
        id: id,
        price: x.price,
        time: x.time,
      };
    });
  }

  componentDidMount() {
    this.sock = new WebSocket("ws://localhost:18080/ws");

    this.sock.onmessage = (e) => {
      if (e.data) {
        let items = JSON.parse(e.data);
        items?.forEach((s) => {
          let newState = this.addElement(s);
          newState.showData = this.constructShowList(newState);
          this.setState(() => newState);
        });
      }
    };
  }

  componentWillUnmount() {
    this.sock.close();
  }

  render() {
    return (
      <div className="centered">
        <TableContainer>
          <Table
            className="table-width table-margin"
            aria-label="customized table"
          >
            <TableHead>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[
                    5,
                    10,
                    25,
                    50,
                    100,
                    { label: "All", value: -1 },
                  ]}
                  colSpan={2}
                  count={this.state.showData.length}
                  rowsPerPage={this.state.rowsPerPage}
                  page={this.state.page}
                  SelectProps={{
                    inputProps: { "aria-label": "rows per page" },
                    native: true,
                  }}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
              <TableRow>
                <StyledTableCell align="center">Time</StyledTableCell>
                <StyledTableCell align="center">Price</StyledTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {(this.state.rowsPerPage > 0
                ? this.state.showData.slice(
                    this.state.page * this.state.rowsPerPage,
                    this.state.page * this.state.rowsPerPage +
                      this.state.rowsPerPage
                  )
                : this.state.showData
              ).map((row) => {
                let priceClass = "";
                if (this.props?.targetValue.current.value < row.price) {
                  priceClass = "price-green";
                } else if (this.props?.targetValue.current.value > row.price) {
                  priceClass = "price-red";
                }

                return (
                  <StyledTableRow key={row.id}>
                    <StyledTableCell component="th" align="center" scope="row">
                      {row.time}
                    </StyledTableCell>
                    <StyledTableCell align="center" className={priceClass}>
                      {row.price}
                    </StyledTableCell>
                  </StyledTableRow>
                );
              })}
            </TableBody>
            <TableFooter>
              <TableRow>
                <TablePagination
                  rowsPerPageOptions={[
                    5,
                    10,
                    25,
                    50,
                    100,
                    { label: "All", value: -1 },
                  ]}
                  colSpan={2}
                  count={this.state.showData.length}
                  rowsPerPage={this.state.rowsPerPage}
                  page={this.state.page}
                  SelectProps={{
                    inputProps: { "aria-label": "rows per page" },
                    native: true,
                  }}
                  onChangePage={this.handleChangePage}
                  onChangeRowsPerPage={this.handleChangeRowsPerPage}
                  ActionsComponent={TablePaginationActions}
                />
              </TableRow>
            </TableFooter>
          </Table>
        </TableContainer>
      </div>
    );
  }
}

function TablePaginationActions(props) {
  const classes = useStyles();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.pagnation}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

export default withStyles(useStyles, { withTheme: true })(TableComponent);
