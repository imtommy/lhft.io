Notes:

Websocket will be used to stream elements update to Frontend once connected.

Backend server will use port 18080.
Backend stores historic values is prepared for query for the last 5 minutes.(Not Yet adding the api and display to Frontend)

Frontend:

- Made by React

Build Steps:

- cd frontend/my-app
- npm install
- npm run start

---

Backend:

- Made by Crow C++ (https://github.com/ipkn/crow)

Build Steps:

- Install Visual Studio 19
- Open backend.vsxproj
- Build Debug/Release
- Copy config/ folder under the same folder with backend.exe
  i.e.

  Debug/
  --backend.exe
  --config/
  ----config.txt

  Release/
  --backend.exe
  --config/
  ----config.txt

- Start backend.exe

---

Remarks:
config.txt stores the configurations for the application.

{
"symbols": [
"AAAA",
"BBBB",
"CCCC",
"DDDD"
],
"update_frequency_milliseconds": 300,
"elements_per_update": 50
}
